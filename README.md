#UserMatch Microservice ENDPOINTS #


Simple NodeJs Microservice that allows you to store and manage users in a mongo database. 
Each user contains a set of tags that represents their hobbies. 
You can search users who have more affinity with you by passing a set of hobbies due to mongo allows you to aggregate and sort user by number of matches with your hobbies.



```
#!Javascript


GET 'http://localhost:8080' ---> "Hello World!"



POST 'http://localhost:8080/users' ---> Add User
formParams:{
   id: String,
   name: String,
   nickName: String,
   age: int,
   password: String,
   tags: String[]
}


POST 'http://localhost:8080/users/find' ---> get Users By Tags
formParams:{
   tags: String[]
}


GET 'http://localhost:8080/users/:userId' ---> get User By Id


POST 'http://localhost:8080/users/:userId' ---> update User Info
formParams:{
   name: String,
   nickName: String,
   age: int
}


DELETE 'http://localhost:8080/users/:userId' ---> delete User


POST 'http://localhost:8080/users/:userId/password' ---> update User Password
formParams:{
   oldPassword: String,
   newPassword: String
}


POST 'http://localhost:8080/users/:userId/tags' ---> update User Tags
formParams:{
   tags: String[]
}

```
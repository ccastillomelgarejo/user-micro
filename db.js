var mongoose = require('mongoose');
var config = require('./config');

var DATABASE_URL = config.database.uri;

mongoose.connect(DATABASE_URL, function(err) {
    if(err)
        console.log(err);
	else
        console.log("Connected to database!");
});
var bodyParser = require('body-parser');
var express = require('express');
var app = express();
require('./db');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var user_micro = require('./user-micro');

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.post('/users', user_micro.addUser);

app.post('/users/find', user_micro.getUsersByTags);

app.get('/users/:userId', user_micro.getUserById);

app.post('/users/:userId', user_micro.updateUserInfo);

app.delete('/users/:userId', user_micro.deleteUser);

app.post('/users/:userId/password', user_micro.updateUserPassword);

app.post('/users/:userId/tags', user_micro.updateUserTags);


var port = 8080;
app.listen(process.env.PORT || port, function(){
    console.log('UserMicro server listening on port ' + port);
});
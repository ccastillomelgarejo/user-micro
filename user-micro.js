var User = require("./models/user").User;
var ERROR_CODE = 500;
var OK_CODE = 200;

function addUser(req, res){

    var id = req.body.id;
    var name = req.body.name;
    var nickName = req.body.nickName;
    var password = req.body.password;
    var age = req.body.age;
    var tags = req.body.tags;

    User.createNewUser(id,name,nickName,password,age,tags,function(err,user){
        if (err) {
            return res.status(ERROR_CODE).end(err);
        } else {
            return res.status(OK_CODE).send(user);
        }
    });

}

function updateUserInfo(req, res){

    var id = req.params.userId;
    var name = req.body.name;
    var nickName = req.body.nickName;
    var age = req.body.age;

    User.updateUserInfo(id,name,nickName,age,function(err,user){
        if (err) {
            return res.status(ERROR_CODE).send(err);
        } else {
            return res.status(OK_CODE).send(user);
        }
    });

}

function updateUserTags(req, res){

    var id = req.params.userId;
    var tags = req.body.tags;

    User.updateUserTags(id,tags,function(err,user){
        if (err) {
            return res.status(ERROR_CODE).send(err);
        } else {
            return res.status(OK_CODE).send(user);
        }
    });

}


function updateUserPassword(req, res){

    var id = req.params.userId;
    var oldPassword = req.body.oldPassword;
    var newPassword = req.body.newPassword;

    User.updateUserPassword(id,oldPassword,newPassword,function(err,user){
        if (err) {
            return res.status(ERROR_CODE).send(err);
        } else {
            return res.status(OK_CODE).send(user);
        }
    });

}

function getUsersByTags(req, res){

    var id = req.params.userId;
    var tags = req.body.tags;

    User.getUsers(id,tags,function(err,user){
        if (err) {
            return res.status(ERROR_CODE).send(err);
        } else {
            return res.status(OK_CODE).send(user);
        }
    });

}

function getUserById(req, res){
    var id = req.params.userId;
    User.getUserById(id,function(err,user){
        if (err) {
            return res.status(ERROR_CODE).end();
        } else {
            return res.status(OK_CODE).send(user);
        }
    });

}

function deleteUser(req, res){
    var id = req.params.userId;

    User.deleteUser(id,function(err,user){
        if (err) {
            return res.status(ERROR_CODE).send(err);
        } else {
            return res.status(OK_CODE).end();
        }
    });

}




exports.addUser = addUser;
exports.updateUserInfo = updateUserInfo;
exports.updateUserTags = updateUserTags;
exports.updateUserPassword = updateUserPassword;
exports.getUsersByTags = getUsersByTags;
exports.getUserById = getUserById;
exports.deleteUser = deleteUser;

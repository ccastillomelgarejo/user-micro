var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = new Schema({
    id: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },    
    nickName: {
        type: String,
        required: true
    },    
    password: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: false
    },
    tags:{ type : Array , "default" : [] }


}, { collection : 'users' });


User.statics.createNewUser = function(id, name,nickName,password,age,tags, callback) {
    this.create({id : id, name : name, nickName : nickName, password:password, age:age,tags:tags }, function(err, user) {
        if (err) {
            return callback(err, null);
        } else {
            return callback(null, user);
        }     
    });
}

User.statics.updateUserInfo = function(id, name,nickName,age, callback) {
    this.findOne({id:id},function(err, user) {
        if (err) {
            return callback(err, null);
        } else {
            user.name=name;
            user.nickName=nickName;
            user.age=age;
            user.save(function(err){
                if (err) {
                    return callback(err, null);
                }else{
                    return callback(null, user);
                }
            });
        }     
    });
}

User.statics.updateUserTags = function(id, tags, callback) {
    this.findOne({id:id},function(err, user) {
        if (err) {
            return callback(err, null);
        } else {
            user.tags=tags;
            user.save(function(err){
                if (err) {
                    return callback(err, null);
                }else{
                    return callback(null, user);
                }
            });
        }     
    });
}

User.statics.updateUserPassword = function(id, oldPassword,newPassword, callback) {
    this.findOne({id:id},function(err, user) {
        if (err) {
            return callback(err, null);
        } else {

            if(user.password != oldPassword)
                return callback("invalid password", null);

            user.password=newPassword;
            user.save(function(err){
                if (err) {
                    return callback(err, null);
                }else{
                    return callback(null, user);
                }
            });
        }     
    });
}


User.statics.getUsers = function(id, userTags, callback) {
    this.aggregate([
        {$match: { tags: {$in:userTags}}},
        {$unwind: "$tags"},
        {$match: { tags: {$in:userTags}}},
        {$group: {
            _id:{id:"$id"},
            matches:{$sum:1}
        }},  
        {$sort:{matches:-1}}
    ],function(err, result) {
        if (err) {
            return callback(err, null);
        } else {
            return callback(null, result);
        }     
    });
}

User.statics.getUserById = function(id, callback) {
    this.findOne({id:id},function(err, user) {
        if (err) {
            return callback(err, null);
        } else {
            return callback(null, user);
        }     
    });
}

User.statics.deleteUser = function(id, callback) {
    this.findOne({id:id},function(err, user) {
        if (err) {
            return callback(err, null);
        } else {
            user.remove();
            return callback(null, "Ok");
        }     
    });
}



module.exports  = {
    User : mongoose.model('User', User)
};